'use strict';

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'nested-components',
    environment: environment
  };

  return ENV;
};
